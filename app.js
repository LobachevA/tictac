window.onload = function () {

    var hod = 0, dimension = 3;
    var query = getURLParameter('hod');

    if (query !== null) {
        hod = Number(query);
        dimension = getURLParameter('dim');
        var historyX = getURLParameter('hx');
        if (historyX !== null) {
            historyX = historyX.split(",");
        }
        var historyO = getURLParameter('ho');
        if (historyO !== null) {
            historyO = historyO.split(",");
        }
        renderGame(dimension,historyO,historyX);

    }

    document.getElementById('link').onclick = generateLink;

    document.getElementById('drow').onclick = function () {
        hod = 0;
        dimension = document.getElementById('number').value;
        renderGame(dimension);
    };


    function checkWinner() {
        var allCells = document.getElementsByClassName('cell');
        var arrayX = [], arrayO = [], flag = false;
        for (var i = 0; i < allCells.length; i++) {
            if (allCells[i].innerHTML === 'x') {
                arrayX.push(allCells[i].id);
            } else if (allCells[i].innerHTML === '0') {
                arrayO.push(allCells[i].id);
            }
        }
        if (arrayX.length >= dimension) {
            flag = checkCombination(arrayX);
            if (flag) {
                alert('X wins');
                document.getElementById('game').onclick = function () {
                };
            }
        }
        if (arrayO.length >= dimension) {
            flag = checkCombination(arrayO);
            if (flag) {
                alert('O wins');
                document.getElementById('game').onclick = function () {
                };
            }
        }

    }

    function checkCombination(takenCells) {
        var arrayRows = [], arrayColumns = [], plusInd = 0, countR = 0, countC = 0;
        for (var k = 1, l = dimension; k <= dimension; k++, l--) {
            for (var m = 0; m < takenCells.length; m++) {
                if (takenCells[m] === String(k + '+' + k)) {
                    countR++;
                }
                if (takenCells[m] === String(l + '+' + k)) {
                    countC++;
                }
            }
        }

        if ((Number(countR) === Number(dimension)) || (Number(countC) === Number(dimension))) {
            return true;
        }
        for (var i = 0; i < takenCells.length; i++) {
            plusInd = takenCells[i].indexOf("+");
            arrayRows.push(takenCells[i].slice(0, plusInd));
            arrayColumns.push(takenCells[i].slice(plusInd + 1));
        }

        for (var j = 1; j <= dimension; j++) {
            countR = countC = 0;
            for (var n = 0; n < arrayRows.length; n++) {
                if (arrayRows[n] === String(j)) {
                    countR++;
                }
                if (arrayColumns[n] === String(j)) {
                    countC++;
                }
            }
            if ((Number(countR) === Number(dimension)) || (Number(countC) === Number(dimension))) {
                return true;
            }
        }
        if (takenCells.length === Number(dimension*dimension)) {
            alert('Ничья');
        }
        return false;
    }

    function cellClick(event) {
        if (event.toElement.innerHTML === '') {
            if (hod % 2 === 0) {
                event.target.innerHTML = 'x';
            }
            else {
                event.target.innerHTML = '0';
            }
            hod++;
            checkWinner();
        }
    }

    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }

    function renderGame(dim, ho, hx ) {
        ho = ho || null;
        hx = hx || null;
        var width = dim * 82;
        var innHtml = '';
        var k = 0;
        document.getElementById('game').innerHTML = innHtml;
        for (var i = 1; i <= dim; i++) {
            innHtml += '<div style="max-width:' + width + 'px">';
            for (var j = 1; j <= dim; j++) {
                k++;
                if(ho && ho.indexOf(String(k)) > -1) {
                    innHtml += '<div class="cell" id="' + i + '+' + j + '">0</div>';
                } else if (hx && hx.indexOf(String(k)) > -1){
                    innHtml += '<div class="cell" id="' + i + '+' + j + '">x</div>';
                } else {
                    innHtml += '<div class="cell" id="' + i + '+' + j + '"></div>';
                }
            }
            innHtml += '</div>';
        }
        document.getElementById('game').innerHTML += innHtml;
        document.getElementById('game').onclick = cellClick;
    }

    function generateLink() {
        var link = [location.protocol, '//', location.host, location.pathname].join('') +'?hod=' + hod + '&dim=' + dimension;
        var filledO = [];
        var filledX = [];
        var plusInd = 0;
        var cellRow = 0;
        var cellCol = 0;
        var allCells = document.getElementsByClassName('cell');
        var temp = '';
        for (var i = 0; i < allCells.length; i++) {

            if (allCells[i].innerHTML === 'x') {
                temp = allCells[i].id;

                plusInd = temp.indexOf("+");
                cellRow = temp.slice(0, plusInd);
                cellCol = temp.slice(plusInd + 1);
                filledX.push(Number((cellRow - 1)*dimension + cellCol ));
            } else if (allCells[i].innerHTML === '0') {
                temp = allCells[i].id;
                plusInd = temp.indexOf("+");
                cellRow = temp.slice(0, plusInd);
                cellCol = temp.slice(plusInd + 1);
                filledO.push(Number((cellRow - 1)*dimension + cellCol ));
            }
        }
        filledX = filledX.join(',');
        filledO = filledO.join(',');
        link += '&ho='+filledO+'&hx='+filledX;
        alert(link);
    }
};
